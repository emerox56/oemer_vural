$(function() {
  $('[data-toggle="tooltip"]').tooltip()
})

$('#myModal').on('shown.bs.modal', function() {
  $('#myInput').trigger('focus')
})
$(document).ready(function() {

  const $valueSpan = $('.valueSpan2');
  const $value = $('#customRange12');
  $valueSpan.html($value.val());
  $value.on('input change', () => {

    $valueSpan.html($value.val());
  });
});
$(document).ready(function() {

  const $valueSpan = $('.valueSpan1');
  const $value = $('#customRange11');
  $valueSpan.html($value.val());
  $value.on('input change', () => {

    $valueSpan.html($value.val());
  });
});

function doCalc() {
  var value1 = document.getElementById("customRange12").value;
  var value2 = document.getElementById("customRange11").value;
  var total = Number(value1) / Number(value2);
  var total = total.toFixed(2);
  document.getElementById("output").value = total;
}

$('.carousel').carousel({
  interval: 4000,
  pause: 'hover'
});
