const http = require('http');
const path = require('path');
const express = require('express');
const app = express();
const fs = require('fs');

app.use(express.static(path.join(__dirname, 'public')));

app.use(express.static(path.join(__dirname, 'public/Aufgabe_Bootstrap')));

app.use(express.json());
app.use(express.urlencoded({extended : true}));

const hostname = 'localhost';
const port = 3000;

const server = http.createServer(app);

app.get('/', function (req, res){
  res.set('Content-Type', 'text/html');
  fs.readFile('./public/Aufgabe_Bootstrap/Aufgabe_Bootstrap.html' ,(error,data) => {
    if(error){
      return res.status(500).send(error);
    }
    res.end(data);
  });
});

server.listen(port, () => {
  console.log('Server running on' + port);
});
