const p = require('path');
//bindest zweite datei ein, bei dateien muss man immer den pfad angeben z.b. ./blabla
const router = require('./router');
const express = require("express");
const app = express();
const users = [];

//middleware ganz am anfang
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

app.get('/', function(req, res) {
  //sendet text zurück
  res.send('Hello World')
})
//get soll iwas ausgeben
app.get('/user', function(req, res) {
  //liefert maschinell lesbare dateien zurück
  res.json(users)
});
// fügt neuen eintrag hinzu
app.post('/user', function(req, res) {
  users.push(req.body);
  res.status(201).send('User created');
});
//verändert/bearbeitet bestehenden eintrag
app.put('/user/:index', function(req, res) {
  if (isNaN(parseInt(req.params.index)) || req.params.index < 0 || req.params.index >= users.length) {
    res.status(404).send("Users not found");
    return;
  } else {
    users[parseInt(req.params.index)] = req.body;
    res.send("Done");
  }

});
//löscht eintrag
app.delete('/user/:index', function(req, res) {
  if (isNaN(parseInt(req.params.index)) || req.params.index < 0 || req.params.index >= users.length) {
    res.status(404).send("Users not found");
    return;
  } else {
    users.splice(parseInt(req.params.index), 1);
    res.send("Deleted");
  }
});

app.listen(3000, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log("Sever running");
  }
})
