package omerSpiel;

import java.util.Random;
import java.util.Scanner;

public class OmerSpiel {

	public static void main(String[] args) {
       
		//Random und input basiertes Spiel
		Scanner in = new Scanner(System.in);
		Random rand = new Random();


		//Sp�ter versuche einzelne Gegner herauszupicken und buffen
        //Bsp: Minotaurus (maxEnemyAttack= 50), Dunkel Magier (maxEnemyHealth=150)
		//HEL zum Bossgegner machen
		// versuchen, dass sie erst kommt wenn man 10 gegner besiegt hat.
		//Scorboard einbauen.
		//z�hler einabauen der nachdem mann einen gegner get�tet hat punkte dazu bekommen 
		//punkte in scoreboard einbauen
		
		
		//Score
		
		int score=0;
		
		
		//Die Gegner
        String[] enemies ={"Assasine" , "Skellet" ,"Goblin", "Dunkeler Magier","Minotaurus","Hel"};
        int maxEnemyHealth= 80; //maximale Gesundheit des Gegners
        int enemyAttackDamage= 20; //maximale Attacke des Gegners!

        //Der Spieler
        
        int playerHealth= 100;
        int playerAttackDamage= 50;
        int numPotion= 3; //Tr�nke die der Spieler dabei hat
        int potionHealAmount= 30;
        int potionDropChance= 50; //Prozentuelle wahrscheinlichkeit ein Trank zu droppen!

        boolean running = true;


        System.out.println("Willkommen zum Dungeon!");
        System.out.println("***++***++***++***++***++");
        
        tag: //das gesamte spiel l�uft in der while schleife! Also w�hrend der Spieler drinnen ist geht das spiel weiter und wenn der Spieler stirbt geht er raus aus der while schleife
        while(running)
	        {
	        System.out.println("------------------------");
	       
	        String enemy = enemies[rand.nextInt(enemies.length)];
	        int enemyHealth = rand.nextInt(maxEnemyHealth);
	        System.out.println("\t# " + enemy + " ist erschienen #\n");

	         
	        //Auswahlm�glichkeiten Spieler 1.Attacke ,2.Trank, 3. Run Bitch Run, issa MONSTAH!!!
		        while( enemyHealth > 0 ) 
		       	{
		        	System.out.println("\t Deine Lebenspunkte: " + playerHealth);//\t bedeutet tab und tabt eine "spalte" rein!
		        	System.out.println("\t Gegner: " + enemy + " Lebenspunkte:" + enemyHealth);
		        	System.out.println("\n\t Was w�rdest du gerne machen? ");
		        	System.out.println("\t 1. Angreifen");
		        	System.out.println("\t 2. Trank trinken, du hast noch " + numPotion);
		        	System.out.println("\t 3. wegrennen :P");
		        	
		        	 
		        	
		
		        	String input = in.nextLine();
		        	if(input.equals("1")) //Sollte der Benutzer 1 eintippen, attackiert er den Gegner und der Gegner attackiert ihn. 
		        	{
		        		int damageDealt= rand.nextInt(playerAttackDamage);//
		        		int damageTaken= rand.nextInt(enemyAttackDamage);//
		        		
		        		
		        		enemyHealth = enemyHealth - damageDealt; // die maximale Gesundheit - hinzugef�gtem schaden = neue gesundheit des Gegners
		        		playerHealth= playerHealth - damageTaken; // beim spieler genau so maximale gesundheit des Spielers - erlittenem Schaden = neue Gesundheit des Spielers
		        		
		        		System.out.println("\t---- Du hast dem Gegner " + enemy +" " + damageDealt + " Schaden hinzugef�gt---------" );
		        		if(damageDealt >= 40) 
		        		{
		        		System.out.println("\t -----------------!VOLTREFFER!-------------------");	
		        		}
		        		System.out.println("\t---- Der Gegner " + enemy +" hat dir " + damageTaken + " Schaden hinzugef�gt---------\n");
		        		
			   		    //SCORE DAZU BEKOMMEN! 
		        		if( enemyHealth <= 0)  
					     {
					    	 score = score +10;
					    	 System.out.println("\t >>>>>>>>>>>>>Du hast 10 Punkte dazu bekommen!<<<<<<<<<<<<<<< \n");
					    	 if(score == 100) 
					    	 {
					    		 System.out.println("\t *YAYYY, DU HAST 100 PUNKTE! NICE!* \n");
					    		 enemy = enemies[5];
					    		 enemyHealth=120;
					    		 System.out.println("\t !WARNING! *WAAAHH HEL IST ERSCHIENEN?!* !WARNING! \n");
					    	 }
					    	
					     }
		        		
		        		//Wenn du keine Lebenspunkte mehr hast, dann setzt "break;" ein und du verl�sst die schleife und somit das spiel.
		        		if(playerHealth < 1)// wenn dein leben unter "0" kommt, wird die schleife automatisch beendet und du kommst so.ohne das break w�rde 
		        		{
		        			System.out.println("\t-----Du hast zu viel Schadden erlitten, du bist zu schwach um weiter zu machen!-------");
		        			break;
		        		}

		        	} 
		        	else if(input.equals("2")) 
		        	{
		        		// wenn die anzahl deiner Tr�nke gr��er als null ist, also wenn du welche hast, dann.....
		        		if(numPotion > 0 ) 
		        		{
		        			//....dann, healthpotion + leben ist gleich neue Lebenspunkte
		        			playerHealth = playerHealth + potionHealAmount;
		        			numPotion--;// und ein existierender Trank weniger.
		        			System.out.println("\t-----Du hast einen Trank getrunken, du hast " + potionHealAmount + " Lebenspunkte dazu bekommen."
		        								+"\n\t----- Du hast jetzt " + playerHealth + " Lebenspukte"
		        								+ "\n\t----- Du hast nur mehr " + numPotion + " Tr�nke bei dir ");
		        		}
		        		else 
		        		{
		        			System.out.println("Du hast keine Tr�nke mehr! Erledige Gegener und droppe villeicht Tr�nke");
		        		}
		        			
		        	}
		        	else if(input.equals("3")) 
		        	{
		        		System.out.println("Du rennst vom Gegner " + enemy + " weg");
		        		continue tag;//wenn du vom Gegner wegrennst, dann beendet er alle Schleifen und geht zum GAME-tag der bei der "haupt-while-schleife" angebracht ist.
		        	}
		        	else 
		        	{
		        		System.out.println("Nichts geschiet");
		        	}
	    	        
		       	}
		        
		        if(playerHealth < 1) 
		        {
		        	System.out.println("Du gehst geschw�cht vom Feld. Entehrt von deinen Gegnern . Man hat dein Gesicht genommen. DU OPFA!\n");
		        	break;
		        }
		        
		        System.out.println("-----------------------");
		        System.out.println(" # " + enemy + " wurde besiegt! #");
		        System.out.println(" # Du hast " + playerHealth + " Lebenspunkte # " );
		        
		        //Trank Drop chance--------
		        if(rand.nextInt(100) < potionDropChance) //wenn die random generierte Zahl kleiner als 100 ist aber gr��er als healthPotioDropChance(50)ist,dann gib mir einen Trank
		        { 
		        	numPotion++;
		        	System.out.println("Der Gegner: " + enemy + " hat einen Trank gedroppt");
		        	System.out.println("Du hast jetzt " + numPotion + " Tr�nke");
		        }
		        
		        System.out.println("----------------");
		        
		        System.out.println("Was w�rdest du gerne tun?");
		        System.out.println("1.Weiterk�mpfen");
		        System.out.println("2.Dungeon verlassen");
		        
		        
		        String input= in.nextLine();
		        while(!input.equals("1") && !input.equals("2")) //wenn die zahl !nicht! 1 oder 2 ist dann mach gar nichts!
		        {
		        	System.out.println("Nichts geschieht");
		        	input = in.nextLine();
		        }
		        
		        if (input.equals("1")) 
		        {
		        	System.out.println("Du k�mpfst also weiter, sehr mutig villeicht ein bisschen dumm, aber mutig :'D ");
		        	
		        }
		        else if(input.equals("2")) 
		        {
		        	System.out.println("Du verl�sst den Dungeon. Ist das Verstand oder Feigheit? Niemand wei� es \n");
		        	break;
		        }
		      
	        }
        	
        	System.out.println("\t >>>>>>>>>>>>>>Du hast insgesamt " + score + "Punkte<<<<<<<<<<<<<<<<<<< \n");
        	System.out.println("++++++++++##########++++++++++");
        	System.out.println("Danke das Sie das Spiel gespielt haben");
        	
  
	}
}
