package engineTester;

import java.util.ArrayList;
import java.util.List;

import java.util.Random;

import constantValues.Constants;
import entities.*;
import guis.GuiRenderer;
import guis.GuiTexture;
import math.Vector2f;
import math.Vector3f;
import terrains.Terrain;
import texture.*;
import model.*;
import renderEngine.*;


//***ThinMatrix***
public class MainGameLoop{
	
	//private static long windowID;
//		private static long variableYieldTime;
//		private static long lastTime;
//		private static Random random = new Random();
		
		
		public static void main(String[] args) {		
			//windowID = Window.createWindow("Project");
			WindowManager window = new WindowManager("Project");
			Loader loader = new Loader();
			
			//RawModel rm = loader.loadToVAO(vertices, textureCoords, indices); 
			
			Light light = new Light(new Vector3f(Constants.LIGHT_X, Constants.LIGHT_Y, Constants.LIGHT_Z), new Vector3f(1, 1, 1));
//			Entity entity = new Entity(tm, new Vector3f(0, 0, -25f), 0,0,0,1);
			
			
			
			
			TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grassy"));
			TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("dirt"));
			TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("grassFlowers"));
			TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("path"));
			
			TerrainTexturePack texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);
			TerrainTexture blendMap = new TerrainTexture(loader.loadTexture("blendmap"));
			
			Terrain terrain = new Terrain(0,-1, loader, texturePack, blendMap, "heightmap");
//			Terrain terrain2 = new Terrain(-1, -1, loader, texturePack, blendMap, "heightmap");
			
			
			MasterRenderer renderer = new MasterRenderer();
			
//			Trees
			RawModel treeRaw = OBJLoader.loadObjModel("lowPolyTree", loader);
			ModelTexture treeTexture = new ModelTexture(loader.loadTexture("lowPolyTree"));
			treeTexture.setShineDamper(20);
			treeTexture.setReflectivity(0);
			TexturedModel treeModel = new TexturedModel(treeRaw, treeTexture);
			
			List<Entity> tree = new ArrayList<Entity>();
			Random random = new Random();
			for(int i=0;i<20;i++){
				float x = random.nextFloat() * 800 - 400;
				float z = random.nextFloat() * -600;
				float y = terrain.getHeightOfTerrain(x, z);
				tree.add(new Entity(treeModel, new Vector3f(x,y,z),0,random.nextFloat()*360,0,0.9f));
//				tree.add(new Entity(treeModel, new Vector3f(random.nextFloat()*800 - 400,0,random.nextFloat() * -600),0,0,0,3));
			}
			
//			Grass
			RawModel grassRaw = OBJLoader.loadObjModel("grassModel", loader);
			ModelTexture grassTexture = new ModelTexture(loader.loadTexture("grassTexture"));
			grassTexture.setShineDamper(20);
			grassTexture.setReflectivity(0);
			TexturedModel grassModel = new TexturedModel(grassRaw, grassTexture);
			grassModel.getTexture().setHasTransparency(true); // to make the object viewable from all sides
			grassModel.getTexture().setUseFakeLighting(true);
			List<Entity> grass = new ArrayList<Entity>();
			Random randomG = new Random();
			for(int i=0;i<500;i++){
				float x = random.nextFloat() * 800 - 400;
				float z = random.nextFloat() * -600;
				float y = terrain.getHeightOfTerrain(x, z);
				grass.add(new Entity(grassModel, new Vector3f(x,y,z),0,random.nextFloat()*360,0,0.9f));
//				grass.add(new Entity(grassModel, new Vector3f(randomG.nextFloat()*800 - 400,0,randomG.nextFloat() * -600),0,0,0,3));
			}
			
//			Fern
			RawModel fernRaw = OBJLoader.loadObjModel("fern", loader);
			ModelTexture fernTextureAtlas = new ModelTexture(loader.loadTexture("fern"));
			fernTextureAtlas.setNumberOfRows(2);
			
			fernTextureAtlas.setShineDamper(50);
			fernTextureAtlas.setReflectivity(0);
			TexturedModel fernModel = new TexturedModel(fernRaw, fernTextureAtlas);
			fernModel.getTexture().setHasTransparency(true);
			
			List<Entity> fern = new ArrayList<Entity>();
			Random rendomF = new Random();
			for(int i=0;i<500;i++){
				if(i%20==0) {
					float x = random.nextFloat() * 800 - 400;
					float z = random.nextFloat() * -600;
					float y = terrain.getHeightOfTerrain(x, z);
//					fern.add(new Entity(fernModel, new Vector3f(x,y,z),0,random.nextFloat()*360,0,0.9f));
					
					fern.add(new Entity(fernModel, random.nextInt(4), new Vector3f(x,y,z),0,random.nextFloat()*360,0,0.9f));
				}
//				fern.add(new Entity(fernModel, new Vector3f(rendomF.nextFloat()*800 - 400,0,rendomF.nextFloat() * -600),0,0,0,3));
			}
			
////			flower
//			RawModel flowerRaw = OBJLoader.loadObjModel("flower", loader);
//			ModelTexture flowerTexture = new ModelTexture(loader.loadTexture("flower"));
//			
//			flowerTexture.setShineDamper(20);
//			flowerTexture.setReflectivity(1);
//			TexturedModel flowerModel = new TexturedModel(flowerRaw, flowerTexture);
//			flowerModel.getTexture().setHasTransparency(true);
//			
//			List<Entity> flower = new ArrayList<Entity>();
//			Random randomFlower = new Random();
//			for(int i=0;i<50;i++){
//				flower.add(new Entity(flowerModel, new Vector3f(randomFlower.nextFloat()*800 - 400,0,randomFlower.nextFloat() * -600),0,0,0,3));
//			}
			
			
			Player player = new Player(window, fernModel, new Vector3f(100,0, -50), 0, 0, 0, 1);
			Camera camera = new Camera(window.getWindowID(), player);

//			GUI
//			List<GuiTexture> guis = new ArrayList<GuiTexture>();
//			GuiTexture gui = new GuiTexture(loader.loadTexture("�M_JavaGame_Logo"), new Vector2f(0.5f, 0.5f), new Vector2f(0.25f, 0.25f));
//			guis.add(gui);
			
			GuiRenderer guiRenderer = new GuiRenderer(loader);
			
			//Main Game Loop
			int gridX = (int) (player.getPosition().x / constantValues.Constants.TERRAIN_SIZE + 1);
			int gridZ = (int) (player.getPosition().z / constantValues.Constants.TERRAIN_SIZE + 1);
			while(!window.shouldClose()) {
				//entity.increasePosition(0.0000f, 1f, 0f);
//				entity.increaseRotation(0f, 1f, 0f);
				
				player.move(terrain/*[gridX][]*/);
				camera.move();
		
//				renderer.processTerrain(terrain2);
				renderer.processTerrain(terrain);
				for(Entity entity:tree) {
					renderer.processEntity(entity);
				}
				for(Entity entity:grass) {
					renderer.processEntity(entity);
				}
				for(Entity entity:fern) {
					renderer.processEntity(entity);
				}
//				for(Entity entity:flower) {
//					renderer.processEntity(entity);
//				}
				
				renderer.processEntity(player);
				
				renderer.render(light, camera);
				
//for GUI rendering				guiRenderer.render(guis);
				
				window.update();		
			}
			
			guiRenderer.cleanUp();
			window.cleanUp();
			renderer.cleanUp();
			loader.cleanUp();		
		}		
}