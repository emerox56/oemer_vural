package renderEngine;

import static org.lwjgl.glfw.GLFW.*;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.NULL;

import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import constantValues.Constants;

public class WindowManager {
//	private int width, height;
//	private String title;
	private long window;
	public int frames;
	public static long time;
	
	private long lastFrameTime;
	private long lostTime = System.nanoTime();
	private long currTime = lostTime;
	private long timer = System.currentTimeMillis();
	private double delta = 0.0;
	private int fps = 0;
	private int ups = 0;
//	private boolean inWire = false;
	
//	private int framecount = 0;
	
////	ContextAttribs attribs = new ContextAttribs(3, 2).withForwardCompatible(true).withProfileCore(true);
	public WindowManager(String title) {
		this.window = createWindow(title);
	}
	
	public long getWindowID() {
		return window;
	}
	
	public boolean shouldClose() {
		if (!glfwWindowShouldClose(window))
			return false;
		return true;
	}
	
	public void hide() {
		glfwHideWindow(window);
	}
	
	public void show() {
		glfwShowWindow(window);
	}
	
	public void cleanUp() {
		glfwHideWindow(window);
		glfwDestroyWindow(window);
	}
	
	public void update() {
    	glfwSwapBuffers(window);
    	updateTimer();
	}
    	
	private long createWindow(String title) {
		glfwInit();
		int display_width = Constants.DISPLAY_WIDTH;
		int display_height = Constants.DISPLAY_HEIGHT;
		
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_SAMPLES, GL_DONT_CARE);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		glfwWindowHint(GLFW_VISIBLE, GL_TRUE);
		glfwWindowHint(GLFW_DECORATED, GL_TRUE);
		glfwWindowHint(GLFW_FOCUSED, GL_TRUE);
		
		long windowID = glfwCreateWindow(display_width, display_height, title, NULL, NULL);
		
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		int x = (vidmode.width() - display_width) / 2;
		int y = (vidmode.height() - display_height) / 2;
		glfwSetWindowPos(windowID, x, y);		
		
		glfwMakeContextCurrent(windowID);
		glfwSwapInterval(1);
		GL.createCapabilities();
		
		return windowID;
	}
	
	public void setTitle(String title) {
		glfwSetWindowTitle(window, title);
	}
	
	public float getDelta() {
    	return (float) delta;
    }
	
	private void updateTimer() {    	
		currTime = System.nanoTime();
		delta += (currTime - lostTime) / Constants.NS;
		lostTime = currTime;	
		//System.out.println(delta);
		while (delta >= 1.0) {
			glfwPollEvents();
			ups++;
			delta--;
		}
		fps++;		
		if (System.currentTimeMillis() > timer + 1000) {
			setTitle("ups: " + ups + ", fps: " + fps);
			ups = 0;
			fps = 0;
			timer += 1000;
		}
	}
    	
    	
//	public void createWindow() {
//		
//		if (!GLFW.glfwInit()) {
//			System.err.println("ERROR: GLFW wasn't initializied");
//			return;
//		}
//		
//		window = GLFW.glfwCreateWindow(width, height, title, 0, 0);
//		
//		if (window == 0) {
//			System.err.println("ERROR: Window wasn't created");
//			return;
//		}
//		
//		GLFWVidMode videoMode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor());
//		GLFW.glfwSetWindowPos(window, (videoMode.width() - width) / 2, (videoMode.height() - height) / 2);
//		GLFW.glfwMakeContextCurrent(window);
//		
////		createCallbacks();
//		
//		GLFW.glfwShowWindow(window);
//		
//		GLFW.glfwSwapInterval(1);
//		
//		time = System.currentTimeMillis();
//	}
//	
//	public void updateWindow() {
//		GLFW.glfwPollEvents();
//		frames++;
//		if (System.currentTimeMillis() > time + 1000) {
//			GLFW.glfwSetWindowTitle(window, title + " | FPS: " + frames);
//			time = System.currentTimeMillis();
//			frames = 0;
//		}
//	}
//	
//	public void swapBuffers() {
//		GLFW.glfwSwapBuffers(window);
//	}
//	
//	public boolean shouldClose() {
//		return GLFW.glfwWindowShouldClose(window);
//	}
//	
//	public void closeWindow() {
//		GLFW.glfwWindowShouldClose(window);
//		GLFW.glfwDestroyWindow(window);
//		GLFW.glfwTerminate();
//	}
	
}
